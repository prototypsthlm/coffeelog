Server::Application.routes.draw do

  #get "log/index"

  resources :coffee_drinkers

  get "log/increment", to: 'coffee_drinkers#increment'
  get "log/create_new", to: 'coffee_drinkers#create_new'
  get "log/reset", to: 'coffee_drinkers#reset'


  get "log/week_cups", to: 'coffee_drinkers#week_cups'
  get "log/cup_count", to: 'coffee_drinkers#cup_count'
  get "log/new_cups", to: 'coffee_drinkers#new_cups'


  root :to => 'coffee_drinkers#index'


resources :cup_of_coffees

  #get 'log/:card_uid', to: 'log#drinker'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
