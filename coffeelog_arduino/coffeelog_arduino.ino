/**************************************************************************/
/*! 
    @file     readMifare.pde
    @author   Adafruit Industries
	@license  BSD (see license.txt)

    This example will wait for any ISO14443A card or tag, and
    depending on the size of the UID will attempt to read from it.
   
    If the card has a 4-byte UID it is probably a Mifare
    Classic card, and the following steps are taken:
   
    - Authenticate block 4 (the first block of Sector 1) using
      the default KEYA of 0XFF 0XFF 0XFF 0XFF 0XFF 0XFF
    - If authentication succeeds, we can then read any of t  he
      4 blocks in that sector (though only block 4 is read here)
	 
    If the card has a 7-byte UID it is probably a Mifare
    Ultralight card, and the 4 byte pages can be read directly.
    Page 4 is read by default since this is the first 'general-
    purpose' page on the tags.


    This is an example sketch for the Adafruit PN532 NFC/RFID breakout boards
    This library works with the Adafruit NFC breakout 
      ----> https://www.adafruit.com/products/364
 
    Check out the links above for our tutorials and wiring diagrams 
    These chips use I2C to communicate

    Adafruit invests time and resources providing this open source code, 
    please support Adafruit and open-source hardware by purchasing 
    products from Adafruit!
*/
/**************************************************************************/
#include <Wire.h>
#include <Adafruit_NFCShield_I2C.h>
#include <SPI.h>
#include <Ethernet.h>
#include <LiquidCrystal.h>
#include <avr/wdt.h>

const char RS_OK = '1';
const char RS_ST[2] = {'$', '='};
const char RS_ER = '0';
const char RS_NW = '2';


#define IRQ   (2)
#define RESET (3)  // Not connected by default on the NFC Shield

Adafruit_NFCShield_I2C nfc(IRQ, RESET);

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//IPAddress ip(192,168,1,177);

// Enter the IP address of the server you're connecting to:
//IPAddress hostname(10,0,1,176);
//int serverPort = 3000;

const char hostname[] = "prototypcoffeelog.herokuapp.com";
//const char hostname[] = "735829.ngrok.com";
int serverPort = 80;

int encoder0PinA = 2;
int encoder0PinB = 9;
int resetPin = 12;

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 23 is default for telnet;
// if you're using Processing's ChatServer, use  port 10002):
EthernetClient client;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(7, 6, 8, 3, 4, 5);

void setupLcd() {
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
}

void printToLcd(String s, String s2)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(s);
  
  if(s2 != NULL)
  {
    lcd.setCursor(0,1);
    lcd.print(s2);
  }  
}



void printToLcdBothRows(char * s, int length)
{
  
  lcd.clear();
  
  int rowLength = 16;
  int rows = 2;
  
  //int length = sizeof(s);
  Serial.println(length);
  //char chars[length];
  //s.toCharArray(chars, length);
  int charIterator = 0;
  
  for(int y = 0; y < rows; y++)
  {
    for(int x = 0; x < rowLength; x++)
    {
      if(charIterator < length)
      {
        if(s[charIterator] == '|')
        {
          charIterator++;
          break;
        }
        else
        {
          lcd.setCursor(x,y);
          lcd.print(s[charIterator]);
          charIterator++;
        }
      }
    }
  }

}

void setupRfid()
{
  printToLcd("init RFID ...", NULL);
  Serial.println("setup rfid!");
  nfc.begin();
  
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  
  Serial.println("Waiting for an ISO14443A Card ...");
}

boolean setupEthernet(void)
{
  printToLcd("init DHCP ...", NULL);
  Serial.println("setup ethernet!");
// Open serial communications and wait for port to open:
  // this check is only needed on the Leonardo:
   //while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  //}

  Serial.println("Getting ip from DHCP");

  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    return false;
  }
  else
  {
    Serial.print("IP address: ");
    for (byte thisByte = 0; thisByte < 4; thisByte++) {
      // print the value of each byte of the IP address:
      Serial.print(Ethernet.localIP()[thisByte], DEC);
      Serial.print("."); 
    }
    Serial.println();
    return true;
  }

}

void setup(void) {
  delay(50); // Give the ethernet shield a chance to come out of reset

  Serial.begin(115200);
  setupLcd();
  setupRfid();
  
  while(!setupEthernet())
  {
    printToLcd("DHCP Failed ...", "Retrying in 5");
    delay(5000);
  }
  printToLcd("Ready!11", NULL);
  
  pinMode (encoder0PinA,INPUT);
  pinMode (encoder0PinB,INPUT);
}

void waitForData(int ms) {
  int delay_time = 200;
  int waiting_periods = ms / delay_time;
  while(!client.available()) {
      if (waiting_periods == 0) {
        return;
      }
      delay(200);
      --waiting_periods;
  }
}

/* One of many methods of checking how much available memory the arduino has left
 * This and others found at: http://playground.arduino.cc/Code/AvailableMemory
 */
int freeRam () {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

String getName(void)
{
  int encoder0Pos = 97;
  int encoder0PinALast = LOW;
  int n = LOW;
  int lastEncoderPos = 96;
  long startTime = 0;
  lcd.clear();
  char name[] = "                ";
  int charIndex = 0;
  while(true)
  {
   n = digitalRead(encoder0PinA);
   if ((encoder0PinALast == LOW) && (n == HIGH)) {
     
     if (digitalRead(encoder0PinB) == LOW) {
       encoder0Pos--;

     } else {
       encoder0Pos++;

     }
       if(encoder0Pos < 97)
       {
         encoder0Pos = 97;
       }
       if(encoder0Pos > 122)
       {
         encoder0Pos = 122;
       }
     // reset timer
     if(encoder0Pos != lastEncoderPos)
     {
       Serial.println("reset");
       startTime = millis();
     }
     
     lastEncoderPos = encoder0Pos;
     char s = encoder0Pos;
     Serial.print (encoder0Pos);
     Serial.print ("/");
     
     
     lcd.setCursor(charIndex,0);
     lcd.print(s); // print name
     
   } 
   encoder0PinALast = n;
   
   long diff = millis() - startTime;
   
   if(diff > 5000)
   {
     Serial.println("go");
     
     if(lastEncoderPos != -1)
     {
       name[charIndex] = lastEncoderPos;
     }
     
     lastEncoderPos = -1;
     charIndex++;
     
     Serial.println("reset");
     startTime = millis();
     
     lcd.setCursor(charIndex,0);
     lcd.print('?'); // print name
          
     if(charIndex == 15 || charIndex >= 1 && name[charIndex-1] == ' ' && lastEncoderPos == -1)
     {
       lcd.clear();
       lcd.setCursor(0,0);
       lcd.print("Ditt namn:");
       lcd.setCursor(0,1);       
       lcd.print(name);
       return name;
     }
     
   }
   else
   {
     lcd.setCursor(15,1); // print counter
     lcd.print(diff/1000);
   }

  }
}

boolean getMessageStart(char sign, char buffer[], int & startIndex)
{
  buffer[startIndex] = sign;
  if(buffer[startIndex] == RS_ST[startIndex])
  {
    if(startIndex < sizeof(RS_ST)-1)
    {
      startIndex++;
      return false;
    }
    else
    {
      Serial.println("Message started:");
      return true;
    }
  }
  else
  {
    return false;
  }
}

char getResponse(char sign, char response[], int bufferPointer, int bufferSize)
{
    //Serial.print(sign);
    if(bufferPointer == 0 && sign == RS_ER)
    {
      return RS_ER;
    }
    else if(bufferPointer == 0 && sign == RS_OK)
    {
     return RS_OK;
    }
    else if(bufferPointer == 0 && sign == RS_NW)
    {
      Serial.println(RS_NW);
      return RS_NW;
    }
    else
    {
      response[bufferPointer-1] = sign;
      return RS_OK;
    }

}


char increment(String uid)
{
    // Serial.print("increment(), free ram: "); Serial.println(freeRam());
    
    Serial.println(uid);
    if (client.connect(hostname, serverPort)) 
    {
      Serial.println(hostname);
         
      client.print("GET /log/increment?uid=");
      client.print(uid);
      client.print("&freeMem=");
      client.print(freeRam());
      client.print(" HTTP/1.1\r\nHost: prototypcoffeelog.herokuapp.com\r\n\r\n");
      // client.print(" HTTP/1.1\r\nHost: 735829.ngrok.com\r\n\r\n");

      printToLcd("Loggar...", NULL);
      
      waitForData(30000);

      int bufferSize = 64;
      char response[bufferSize];
      char rs = RS_ER;
      for(int i = 0; i < bufferSize; i++)
      {
        response[i] = ' ';
      }
      
      int bufferPointer = 0;
      
      int sii = 0;
      boolean started = false;
      char startIndicator[sizeof(RS_ST)];

      // Serial.print("client available: ");
      // Serial.println(client.available());

      while(client.available())
      {
        char sign = client.read();
        //Serial.print("read sign: ");
        //Serial.println(sign);
              
        if(started)
        {
          rs = getResponse(sign, response, bufferPointer, bufferSize);
          Serial.println(bufferPointer);
          if(rs != RS_OK || bufferPointer > bufferSize)
          {
            Serial.println("error");
            break;
          }
          else
          {
              bufferPointer++;
          }
        }
        else
        {
          started = getMessageStart(sign, startIndicator, sii);
        } 
      }
    
      if(rs == RS_OK)
      {
        printToLcdBothRows(response, bufferPointer);
      }

      delay(200);

      client.println("Connection: close");
      client.println();
      client.stop();
      return rs;
  } 
  else {
    // kf you didn't get a connection to the server:
    Serial.println("connection failed");
    return RS_ER;
  }
  
}

char createUser(String name, String uid)
{
    Serial.println("create user");
    Serial.println(name);
    Serial.println(uid);
    
    if (client.connect(hostname, serverPort)) 
    {
      Serial.println(hostname);

      Serial.print("the new UID is "); Serial.print(uid); Serial.println(".");
    
      // Make a HTTP request:
      client.print("GET /log/create_new?uid=");
      client.print(uid);
      client.print("&n=");
      for(int i = 0; i < 16; i++)
      {
        if(name[i] != ' ')
        {
          client.print(name[i]);
        }
      }
      client.print(" HTTP/1.1\r\nHost: prototypcoffeelog.herokuapp.com\r\n\r\n");
      // client.print(" HTTP/1.1\r\nHost: 735829.ngrok.com\r\n\r\n");
      
      client.println();
      //client.println("Host: www.google.com");

      printToLcd("Vänta...", NULL);
    
      waitForData(30000);

      int bufferSize = 64;
      char response[bufferSize];
      char rs = RS_ER;
      for(int i = 0; i < bufferSize; i++)
      {
        response[i] = ' ';
      }
      
      int bufferPointer = 0;
      int sii = 0;
      boolean started = false;
      char startIndicator[sizeof(RS_ST)];
      while(client.available())
      {
        
        char sign = client.read();
        if(started)
        {
          rs = getResponse(sign, response, bufferPointer, bufferSize);
          if(rs != RS_OK || bufferPointer > bufferSize)
          {
            break;
          }
          else
          {
              bufferPointer++;
          }
        }
        else
        {
          started = getMessageStart(sign, startIndicator, sii);
        } 
      }
      
      if(rs == RS_OK)
      {
        printToLcdBothRows(response, bufferPointer);
      }
    
      client.println("Connection: close");
      client.println();
      client.stop();
      return rs;
  
    
  } 
  else {
    // kf you didn't get a connection to the server:
    Serial.println("connection failed");
    return RS_ER;
  }
  
}

uint8_t success;
uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  
void loop(void) {

  //Serial.print("free ram start: "); Serial.println(freeRam());
  
  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  
  if (success) {
    
    // Display some basic information about the card
    Serial.println("Found an ISO14443A card");
    //Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
    //Serial.print("  UID Value: ");
    //nfc.PrintHex(uid, uidLength);
    //Serial.println("");
    
    String uidString;
    
    for(int i = 0; i < uidLength; i++)
    {
      uidString += String(uid[i], DEC);
    }

    const char rs = increment(uidString);
    Serial.println("got response");
    Serial.println(rs);
    if(rs == RS_OK)
    {
      delay(5000);
    }
    else if(rs == RS_ER)
    {
      printToLcd("Ett fel uppstod ..", ":(");
    }
    else if (rs == RS_NW)
    {
      printToLcd("Ange namn via", "<-- ratten");
      delay(5000);
      String name = getName();
      delay(2000);
      Serial.println(name);
      const char rs2 = createUser(name, uidString);
      
      if(rs2 == RS_OK)
      {
        printToLcd("Yo " + name, "Testa igen!");
      }
      else
      {
        printToLcd("Ett fel uppstod ..", ":(");
      }
      
    }

    //Serial.print("free ram end: "); Serial.println(freeRam());
    
    /*
    if (uidLength == 4)
    {
      // We probably have a Mifare Classic card ... 
      Serial.println("Seems to be a Mifare Classic card (4 byte UID)");
	  
      // Now we need to try to authenticate it for read/write access
      // Try with the factory default KeyA: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
      Serial.println("Trying to authenticate block 4 with default KEYA value");
      uint8_t keya[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	  
	  // Start with block 4 (the first block of sector 1) since sector 0
	  // contains the manufacturer data and it's probably better just
	  // to leave it alone unless you know what you're doing
      success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4, 0, keya);
	  
      if (success)
      {
        Serial.println("Sector 1 (Blocks 4..7) has been authenticated");
        uint8_t data[16];
		
        // If you want to write something to block 4 to test with, uncomment
		// the following line and this text should be read back in a minute
        // data = { 'a', 'd', 'a', 'f', 'r', 'u', 'i', 't', '.', 'c', 'o', 'm', 0, 0, 0, 0};
        // success = nfc.mifareclassic_WriteDataBlock (4, data);

        // Try to read the contents of block 4
        success = nfc.mifareclassic_ReadDataBlock(4, data);
		
        if (success)
        {
          // Data seems to have been read ... spit it out
          Serial.println("Reading Block 4:");
          nfc.PrintHexChar(data, 16);
          Serial.println("");
		  
          // Wait a bit before reading the card again
          delay(1000);
        }
        else
        {
          Serial.println("Ooops ... unable to read the requested block.  Try another key?");
        }
      }
      else
      {
        Serial.println("Ooops ... authentication failed: Try another key?");
      }
    }
    
    if (uidLength == 7)
    {
      // We probably have a Mifare Ultralight card ...
      Serial.println("Seems to be a Mifare Ultralight tag (7 byte UID)");
	  
      // Try to read the first general-purpose user page (#4)
      Serial.println("Reading page 4");
      uint8_t data[32];
      success = nfc.mifareultralight_ReadPage (4, data);
      if (success)
      {
        // Data seems to have been read ... spit it out
        nfc.PrintHexChar(data, 4);
        Serial.println("");
		
        // Wait a bit before reading the card again
        delay(1000);
      }
      else
      {
        Serial.println("Ooops ... unable to read the requested page!?");
      }
    }
    */
  }
}


