var Highcharts = Highcharts; // make linter happy

var coffee = coffee || {};
coffee.chart = coffee.chart || {};

$.extend(true, coffee.chart, {
	container: "#container",

	helpers: {
    smoothPieUpdate: function (chart, data) {
      $('#highscore-container').html(data.highscoreHtml); // Update the list

      var series = chart.series[0];
      var current_data = series.data;

      // Build a hash of current values for easy lookup
      var current_data_hash = {};
      $(current_data).each(function (index, point) {
        current_data_hash[point.id] = point;
      });

      // Iterate over fresh data. Update existing points, add new ones
      $(data.seriesData).each(function (index, new_point) {
        var existing_point = current_data_hash[new_point.id];
        if (existing_point) {
          if (existing_point.y !== new_point.y) existing_point.update(new_point.y)
        }
        else {
          series.addPoint(new_point);
        }
      });

    },

    smoothAreaUpdate: function (chart, data) {
      $('#highscore-container').html(data.highscoreHtml); // Update the list

      var series = chart.series;

      // Build a hash of series for easy lookup, keyed on user id. A series repesents one user.
      // The data in each series is the number of cups for a particular day for that user.
      var user_hash = {};
      $(series).each(function (index, series) {
        user_hash[series.options.id] = series;
      });

      // Iterate over fresh data, update the most recent series data points.
      $(data.seriesData).each(function (index, fresh_userdata) {
        var user_series = user_hash[fresh_userdata.id];
        if (user_series) {
          var latest_data_index  = user_series.data.length - 1;
          var latest_cup_count = fresh_userdata.data[latest_data_index][1];
          user_series.data[latest_data_index].update(latest_cup_count);
        }
      });

    }
	},

	dynamicArea: function (data, container) {

		$('#highscore-container').html(data.highscoreHtml);

		Highcharts.getOptions().colors = coffee.chart.style.colors.basic;

		$(container).highcharts({
			chart: {
				type: 'areaspline'
			},
			title: {
				text: data.title,
				style: {
					color: '#E9D6C1'
				} 
			},
			subtitle: {
				text: data.subtitle,
				style: {
					color: '#E9D6C1'
				} 
			},
      exporting: { enabled: false },
			legend: {
				layout: 'vertical',
				align: 'left',
				verticalAlign: 'top',
				x: 50,
				y: 50,
				floating: true,
				borderWidth: 0,
				borderColor: '#917950',
				backgroundColor: '#68583B'
			},
			xAxis: {
				categories: [
					'Monday',
					'Tuesday',
					'Wednesday',
					'Thursday',
					'Friday',
					'Saturday',
					'Sunday'
				],
				plotBands: [{ // visualize the weekend
					from: 4.5,
					to: 6.5,
					color: 'rgba(68, 170, 213, .2)'
				}]
			},
			yAxis: {
				title: {
					text: 'Cups of Coffee'
				}
			},
			tooltip: {
				shared: true,
				valueSuffix: ' cups'
			},
			credits: {
				enabled: false
			},
			plotOptions: {
				areaspline: {
					fillOpacity: 0.5
				}
			},
			series: data.seriesData
		});
	},

	dynamicPie: function (data, container) {

		$('#highscore-container').html(data.highscoreHtml);

		// Radialize the colors
		Highcharts.getOptions().colors = coffee.chart.style.colors.radial(coffee.chart.style.colors.basic);
		
		// Build the chart
		$(container).highcharts({
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: data.title,
				style: {
					color: '#E9D6C1'
				} 
			},
			subtitle: {
				text: data.subtitle,
				style: {
					color: '#E9D6C1'
				} 
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
			},
			credits: {
				enabled: false
			},
      exporting: { enabled: false },
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					bordercolor: '#E9D6C1',
					dataLabels: {
						enabled: true,
						color: '#E9D6C1',
						connectorColor: '#E9D6C1',
						format: '<b>{point.name}</b>: {point.percentage:.1f}% ({point.y} cups)',
						// formatter: function() {
						// 	return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(1) +' % ({point.y})';
						// }
					}
				}
			},
			series: [{
				type: 'pie',
				name: 'Portion of total hot boiling joy',
				data: data.seriesData
			}]
		});
	},


	init: function (containerSelector) {
		coffee.chart.container = containerSelector
		coffee.chart.style.theme.coffee.colors = coffee.chart.style.colors.basic;
	}
});