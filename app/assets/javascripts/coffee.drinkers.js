var Highcharts = Highcharts; // make linter happy

var coffee = coffee || {};
coffee.drinkers = coffee.drinkers || {};

$.extend(true, coffee.drinkers, {
  increment_audio: null,
	helper: {
		viewButtonClickClassSwitch: function (target) {
			$('.view-switch .btn').removeClass('btn-primary');
			$(target).addClass('btn-primary');
		},
		chartCleanup: function () {
			clearInterval(coffee.chart.dynamicIntervalId);
			if ($('#container').highcharts() !== undefined) {
				$('#container').highcharts().destroy();
			}
		}
	},

	event: {
		click: {
			area: function () {
				$('#highscore-container').show();
				coffee.drinkers.helper.viewButtonClickClassSwitch(this);
				coffee.drinkers.display.area();
			},
			pie: function () {
				$('#highscore-container').show();
				coffee.drinkers.helper.viewButtonClickClassSwitch(this);
				coffee.drinkers.display.pie();
			},
			highscore: function () {
				$('#highscore-container').show();
				coffee.drinkers.helper.viewButtonClickClassSwitch(this);
				coffee.drinkers.display.highscore();
			}
		}
	},

	display: {
		area: function () {
			coffee.drinkers.helper.chartCleanup();
			$.get('/log/week_cups', function (data) {
				coffee.chart.dynamicArea(data, '#container');
			});
		},
		pie: function () {
			coffee.drinkers.helper.chartCleanup();
			$.get('/log/cup_count', { week: true }, function (data) {
				coffee.chart.dynamicPie(data, '#container');
			});
		},
		highscore: function () {
			coffee.drinkers.helper.chartCleanup();
			$.get('/log/cup_count', { week: false }, function (data) {
				coffee.chart.dynamicPie(data, '#container');
			});
		}
	},

  update: {
    area: function () {
      var chart = $('#container').highcharts();
      $.get('/log/week_cups', function (data) {
        coffee.chart.helpers.smoothAreaUpdate(chart, data);
      });
    },
    pie: function () {
      var chart = $('#container').highcharts();
      $.get('/log/cup_count', { week: true }, function (data) {
        coffee.chart.helpers.smoothPieUpdate(chart, data);
      });
    },
    highscore: function () {
      var chart = $('#container').highcharts();
      $.get('/log/cup_count', { week: false }, function (data) {
        coffee.chart.helpers.smoothPieUpdate(chart, data);
      });
    }
  },

  init: function () {
    $('.show-area').on('click', coffee.drinkers.event.click.area);
    $('.show-pie').on('click', coffee.drinkers.event.click.pie);
    $('.show-highscore').on('click', coffee.drinkers.event.click.highscore);

    coffee.chart.init('#container');

    // Apply the theme
    Highcharts.setOptions(coffee.chart.style.theme.coffee);

    // Subscribe to pusher notifications
    var pusher = new Pusher(gon.pusher_key);
    var channel = pusher.subscribe("new_data");
    channel.bind('refresh', function(data) {

      var audio_tag = $('#increment-audio').get(0);
      audio_tag.load();
      audio_tag.play();

      console.log('New cup for ' + data.drinker);

      var wordz = data.cups > 1 ? 'koppar' : 'kopp';

      flash(data.drinker + ' har tagit ' + data.cups + ' ' + wordz + ' idag');

      var viewing_daily = $('.show-area').hasClass('btn-primary');
      var viewing_weekly = $('.show-pie').hasClass('btn-primary');
      var viewing_highscore = $('.show-highscore').hasClass('btn-primary');

      if (viewing_daily) {
        coffee.drinkers.update.area();
      }
      else if (viewing_weekly) {
        coffee.drinkers.update.pie();
      }
      else if (viewing_highscore) {
        coffee.drinkers.update.highscore();
      }
    });

		$('.show-pie').click();
	}
});