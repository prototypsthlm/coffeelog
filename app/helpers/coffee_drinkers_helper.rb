
module CoffeeDrinkersHelper

	def days_since_week_start
		locale_offset  = 1 # Monday is day 1, haven't found a convenient way of setting locale locally. Probably have to conf it system wide
    	days_since_start_of_week = (Date.today.wday - locale_offset) % 7

    	return days_since_start_of_week
    end

    def week_start
    	return days_since_week_start().days.ago
    end

    def drinkers_with_weeks_cups()
    	drinkers = CoffeeDrinker.all
    	drinkers.each do |drinker|
  			drinker.cup_of_coffees = drinker.cup_of_coffees.select do |cup|
  				cup.created_at >= week_start()
  			end
  			drinker.cup_of_coffees = drinker.cup_of_coffees.where('created_at >= ?', week_start())
  		end
	  	return drinkers
    end

    def highscore_list(since=nil)
    	hs = []
    	CoffeeDrinker.all.each do |drinker|
  			drinker = {
  				name: drinker.name,
  				cups: since.nil? ? drinker.cup_of_coffees.length : drinker.cup_of_coffees.since(drinker.id, since).length,
  				id: drinker.id,
  				card_uid: drinker.card_uid
  			}
  			
  			hs << OpenStruct.new(drinker)
  		end

  		return hs.sort_by { |d| d.cups }.reverse

    end
end
