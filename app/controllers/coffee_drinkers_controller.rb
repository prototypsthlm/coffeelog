#encoding: utf-8
include CoffeeDrinkersHelper

class CoffeeDrinkersController < ApplicationController

	def index
    gon.pusher_key = Pusher.key
		@hs_list = highscore_list(week_start())
	end

  def show
  	puts params[:id]
  	@drinker = CoffeeDrinker.find_by_card_uid(params[:id])
  	@group = @drinker.cup_of_coffees.group('date(created_at)').count
  	@group = @drinker.cup_of_coffees.group_by(&:created_at).map {|k,v| [k, v.length]}.sort
  	@drinklog = @drinker.cup_of_coffees.order('created_at DESC').group_by {|video| video.created_at.to_date }

  end


  
	def increment
		uid = params[:uid]
		
		if uid == ''
			return render :status=>400, :text=> "$=0"
		end
		
		@drinker = CoffeeDrinker.where(card_uid: uid).first
		
		if @drinker.nil?
			
			return render :inline => "$=2"
		end
		

		latest_cup = @drinker.cup_of_coffees.order('created_at DESC').first
		
		if params[:days] # DEBUG HELPER
			newCup = CupOfCoffee.new
			newCup.created_at = DateTime.now - params[:days].to_i
			@drinker.cup_of_coffees << newCup
			puts "OOOOO Added filthy debug coffee for #{@drinker.id} #{newCup.created_at}"

    elsif latest_cup.nil? || latest_cup.created_at < Time.now-50.seconds
      @drinker.cup_of_coffees << CupOfCoffee.new

      selector = "created_at > '%s 04:30:00'" % Time.now.to_date.to_formatted_s(:db)

      cups_today = @drinker.cup_of_coffees.where(selector).count

      Pusher['new_data'].trigger('refresh', {
          drinker: @drinker.name,
          cups: cups_today
      })

			return render :status=>200, :text=> "$=1Hej %s,|%s koppar idag! " % [@drinker.name, cups_today]
		else
			return render :status=>400, :text=> "$=1Hej %s!|Redan loggad! " % [@drinker.name]
		end
		
		
  
  end
  
	def create_new
	
			uid = params[:uid]
			name = params[:n]
			
			@drinker = CoffeeDrinker.where(card_uid: uid).first

			if @drinker.nil? && uid != '' && name != ''
				
				@drinker = CoffeeDrinker.new
				@drinker.card_uid = uid
				@drinker.name = name
				@drinker.save
				return render :status=>200, :text=> "$=1"
			else
				return render :status=>400, :text=> "$=0"
			end
	end
	
	def reset
		CoffeeDrinker.delete_all
		return render :text => "tog bort allt :s"
	end
	
	def week_cups
  	drinkers = CoffeeDrinker.all

  	@hs_list = highscore_list(week_start())
  	highscoreHtml = render_to_string(partial: "highscore", layout: false)

  	render json: {:title => "Coffee Week", :subtitle => "Hot boiling joy since #{week_start.to_date}", :seriesData => drinkers, :highscoreHtml => highscoreHtml}
  end

  def cup_count
  	filter_week = params[:week]
  	@drinkers = CoffeeDrinker.all
  	data = []

  	@drinkers.each do |drinker|
  		datapoint = {
  			name: drinker.name,
  			id: drinker.id,
  			y: filter_week == "true" ? drinker.cup_of_coffees.since(drinker.id, week_start).length.to_f : drinker.cup_of_coffees.length.to_f
  		}

  		data << datapoint
  	end

  	max = data.max_by do |el|
  		el[:y]
  	end

  	index_of_max = data.index(max)
  	data[index_of_max] = {
  		name: max[:name],
  		id: max[:id],
  		y: max[:y],
  		sliced: true,
  		selected: true
  	}
  	@hs_list = filter_week == "true" ? highscore_list(week_start()) : highscore_list
  	highscoreHtml = render_to_string(partial: "highscore", layout: false)

  	title = filter_week == "true" ? "Week Totals"
  																: "Highscore"

  	subtitle = filter_week == "true" ? "Individual share of total hot boiling joy since #{week_start.to_date}"
  																		: "Individual share of total hot boiling joy since FOREVER"
  	returnData = {:title => title, :subtitle => subtitle, :seriesData => data, :highscoreHtml => highscoreHtml}
  	render json: returnData
  end

  # Unused?
  def new_cups
  	timeInt = params[:since] || Time.now
  	sinceTime = Time.at(timeInt.to_i)
  	new_cups = {}
  	CupOfCoffee.where('created_at >= ?', sinceTime).group_by { |cup| cup.coffee_drinker_id }.each do |id, cups|
  		new_cups[id] = cups.length
  	end
  	
  	highscoreHtml = ""
  	if new_cups.length > 0
  		@hs_list = highscore_list()
  		highscoreHtml = render_to_string(partial: "highscore", layout: false)
  	end
  	render json: {newCups: new_cups, highscoreHtml: highscoreHtml}
  end

  def highscore_html
  end


end
