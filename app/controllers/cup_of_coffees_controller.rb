class CupOfCoffeesController < ApplicationController
  before_action :set_cup_of_coffee, only: [:show, :edit, :update, :destroy]

  # # GET /cup_of_coffees
  # # GET /cup_of_coffees.json
  # def index
  #   @cup_of_coffees = CupOfCoffee.all
  # end

  # # GET /cup_of_coffees/1
  # # GET /cup_of_coffees/1.json
  # def show
  # end

  # # GET /cup_of_coffees/new
  # def new
  #   @cup_of_coffee = CupOfCoffee.new
  # end

  # # GET /cup_of_coffees/1/edit
  # def edit
  # end

  # # POST /cup_of_coffees
  # # POST /cup_of_coffees.json
  # def create
  #   @cup_of_coffee = CupOfCoffee.new(cup_of_coffee_params)

  #   respond_to do |format|
  #     if @cup_of_coffee.save
  #       format.html { redirect_to @cup_of_coffee, notice: 'Cup of coffee was successfully created.' }
  #       format.json { render action: 'show', status: :created, location: @cup_of_coffee }
  #     else
  #       format.html { render action: 'new' }
  #       format.json { render json: @cup_of_coffee.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # PATCH/PUT /cup_of_coffees/1
  # # PATCH/PUT /cup_of_coffees/1.json
  # def update
  #   respond_to do |format|
  #     if @cup_of_coffee.update(cup_of_coffee_params)
  #       format.html { redirect_to @cup_of_coffee, notice: 'Cup of coffee was successfully updated.' }
  #       format.json { head :no_content }
  #     else
  #       format.html { render action: 'edit' }
  #       format.json { render json: @cup_of_coffee.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /cup_of_coffees/1
  # DELETE /cup_of_coffees/1.json
  def destroy
    drinker_id = @cup_of_coffee.coffee_drinker_id - 1
    @cup_of_coffee.destroy
    respond_to do |format|
      format.html { redirect_to :controller => "coffee_drinkers", :action => "show", :id => drinker_id }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cup_of_coffee
      @cup_of_coffee = CupOfCoffee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cup_of_coffee_params
      params[:cup_of_coffee]
    end
end
