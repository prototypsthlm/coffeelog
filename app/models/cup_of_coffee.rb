include CoffeeDrinkersHelper

class CupOfCoffee < ActiveRecord::Base
	belongs_to :coffee_drinker
	scope :this_week, where("created_at >= ?", week_start()) # We completely lose the belongs_to relation if we use this. Fuckity fuck.

	def self.since(user, time=nil)
		if time.nil?
			return where("coffee_drinker_id = ?", user)
		end
		where("coffee_drinker_id = ? AND created_at >= ?", user, time)
	end

end
