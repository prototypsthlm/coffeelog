include CoffeeDrinkersHelper

class CoffeeDrinker < ActiveRecord::Base
  has_many :cup_of_coffees

  def week_cups_grouped_by_day
      return get_cup_data_series(week_start())
  end

  def get_cup_data_series (since_date)

    date_range = since_date.to_date..Date.today

    cups = CupOfCoffee.where("coffee_drinker_id = ? AND created_at >= ?", self.id, since_date.beginning_of_day)
    cups = cups.group_by {|cup| cup.created_at.to_date } # Gets a hashmap, date => cups

    data = []
    date_range.each do |day|
      cup_count = cups[day.to_date] == nil ? 0 : cups[day.to_date].length
      data << [day.to_date, cup_count]
    end

    return data;
  end

  def as_json (options={})
    # add whatever fields you need here
    { :name => self.name, :id => self.id, :data => self.week_cups_grouped_by_day }
  end

end
