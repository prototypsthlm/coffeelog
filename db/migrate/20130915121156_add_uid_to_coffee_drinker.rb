class AddUidToCoffeeDrinker < ActiveRecord::Migration
  def change
    add_column :coffee_drinkers, :card_uid, :string
  end
end
