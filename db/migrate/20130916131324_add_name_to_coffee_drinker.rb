class AddNameToCoffeeDrinker < ActiveRecord::Migration
  def change
    add_column :coffee_drinkers, :name, :string
  end
end
