class AddCoffeeReferences < ActiveRecord::Migration
  def change
	
	add_column :cup_of_coffees, :coffee_drinker_id, :integer
    add_index :cup_of_coffees, :coffee_drinker_id
  
  end
end
