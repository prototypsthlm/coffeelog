require 'test_helper'

class CupOfCoffeesControllerTest < ActionController::TestCase
  setup do
    @cup_of_coffee = cup_of_coffees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cup_of_coffees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cup_of_coffee" do
    assert_difference('CupOfCoffee.count') do
      post :create, cup_of_coffee: {  }
    end

    assert_redirected_to cup_of_coffee_path(assigns(:cup_of_coffee))
  end

  test "should show cup_of_coffee" do
    get :show, id: @cup_of_coffee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cup_of_coffee
    assert_response :success
  end

  test "should update cup_of_coffee" do
    patch :update, id: @cup_of_coffee, cup_of_coffee: {  }
    assert_redirected_to cup_of_coffee_path(assigns(:cup_of_coffee))
  end

  test "should destroy cup_of_coffee" do
    assert_difference('CupOfCoffee.count', -1) do
      delete :destroy, id: @cup_of_coffee
    end

    assert_redirected_to cup_of_coffees_path
  end
end
